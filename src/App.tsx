import React, { useReducer } from 'react';
import './App.scss';
import Header from './components/header/Header';
import Counter from './components/counter/Counter';
import { Route, Switch } from 'react-router-dom';
import ArticlesPage from './pages/articles/ArticlesPage';
import ArticlePage from './pages/article/ArticlePage';
import AppContext from './shared/app.context';
import LoginPage from './pages/login/LoginPage';
import RegisterPage from './pages/register/RegisterPage';
import { profileReducer } from './shared/redux/profile/profile.reducer';

const App: React.FunctionComponent = () => {

  // const [state, dispatch] = useReducer(reducer, {
  //   profile: null, 
  // })

  const [state, dispatch] = useReducer(profileReducer, {
    profile:null,
    error: null
  })

  return (
    <AppContext.Provider value={{state, dispatch}}>
      <div className="App">
        <Header />
        <div className='app__container'>
          <Switch>
            <Route path='/counter' render={() => <Counter initialNum={0} />} />
            <Route exact path='/articles' component={ArticlesPage} />
            <Route path='/articles/:categoryId' component={ArticlesPage} />
            <Route path='/article/:articleId' component={ArticlePage} />
            <Route path='/login' component={LoginPage} />
            <Route path='/register' component={RegisterPage} />
          </Switch>
        </div>
      </div>
    </AppContext.Provider>
  );
}

export default App;
