import React, { useEffect, useState } from 'react';
import { validateEmail } from '../../shared/utils';
import styles from './InputField.module.scss';

type Props = {
    label: string;
    name: string;
    value: string;
    type: 'text' | 'password' | 'textarea';
    onChange: (innerValue: string) => void;
    isRequired: boolean;
    placeholder?: string;
}

const InputField: React.FC<Props> = ({ label, name, value, type, onChange, isRequired, placeholder }) => {

    const [innerValue, setInnerValue] = useState<string>(value);
    const [error, setError] = useState<string | null>(null);

    useEffect(() => {
        if (isRequired) {
            if (innerValue.trim().length) {
                if (name === 'email') {
                    if (validateEmail(innerValue)) {
                        setError(null);
                        onChange(innerValue);
                    } else {
                        setError('Please enter a valid email address.');
                    }
                } else {
                    setError(null);
                    onChange(innerValue);
                }
            } else {
                setError('Please fill a required field.');
            }
        } else {
            onChange(innerValue);
        }
    }, [innerValue])

    return (
        <div className={styles.InputField}>
            <label>{label}</label>
            {type === 'textarea' ?
                <textarea
                    name={name}
                    value={innerValue}
                    onChange={(e) => setInnerValue(e.target.value)}
                    placeholder={placeholder} />
                :
                <input
                    name={name}
                    type={type}
                    value={innerValue}
                    onChange={(e) => setInnerValue(e.target.value)}
                    placeholder={placeholder} />
            }
            {error && <label className={styles._lower_label}>{error}</label>}
        </div>
    );
};

export default InputField;