import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import AppContext from '../../shared/app.context';
import { resetProfile } from '../../shared/redux/profile/profile.actions';
import Button from '../header/button/Button';
import styles from './Profile.module.scss';

type Props = {
    username: string;
    avatar: string;
}

const Profile: React.FC<Props> = ({ username, avatar }) => {

    const { dispatch } = useContext(AppContext);
    const history = useHistory();

    const logOut = () => {
        dispatch(resetProfile());
        history.push('/');
    }

    return (
        <div className={styles.header_profile}>
            <h5 className={styles.profile_username}>{username}</h5>
            <div className={styles.profile_image_container}>
                <img className={styles.profile_image} src={avatar} alt={username} />
            </div>
            <Button title={'Log out'} onClick={logOut} />
        </div>
    )
}

export default Profile;