import React, { useEffect, useState } from 'react';
import AppContext from '../../shared/app.context';
import Button from '../header/button/Button';
import InputField from '../input/InputField';
import Profile from '../profile/Profile';
import styles from './CommentForm.module.scss'

const CommentForm: React.FC = () => {
    const [fields, setFields] = useState<{
        username: string;
        email: string;
        comment: string;
    }>({
        username: '',
        email: '',
        comment: ''
    });
    const [isClickable, setIsClickable] = useState<boolean>(false);

    const onSubmit = () => {
        console.log('Form submitted', fields);
    };

    useEffect(() => {
        if (!fields.email.trim().length || !fields.username.trim().length || !fields.comment.trim().length) {
                setIsClickable(false);
            } else {
                setIsClickable(true);
            }
    }, [fields]);

    const fieldChange = (fieldName: string, value: any) => {
        setFields(oldState => ({
            ...oldState,
            [fieldName]: value
        }))
    };

    return (
        <AppContext.Consumer>
            {({ state: { profile } }) => (profile ?
                <div className={styles.wrapper}>
                    <div className={styles.user}>
                        <InputField
                            label={'Логин'}
                            name={'username'}
                            value={fields.username}
                            type={'text'}
                            onChange={(innerValue) => { fieldChange('username', innerValue) }}
                            isRequired={true} />
                        <InputField
                            label={'Email'}
                            name={'email'}
                            value={fields.email}
                            type={'text'}
                            onChange={(innerValue) => { fieldChange('email', innerValue) }}
                            isRequired={true} />
                        <AppContext.Consumer>
                            {({ state: { profile } }) => (profile ? <Profile username={profile?.username} avatar={profile.avatar} /> : '')}
                        </AppContext.Consumer>
                    </div>
                    <div className={styles.comment}>
                        <InputField
                            label={'Комментарий'}
                            name={'comment'}
                            value={fields.comment}
                            type={'textarea'}
                            onChange={(innerValue) => { fieldChange('comment', innerValue) }}
                            isRequired={true} />
                    </div>
                    <div className={styles.controls}>
                        {isClickable && <Button title='Submit' variant='primary' onClick={onSubmit} />}
                    </div>
                </div> :
                'Чтобы оставить комментарий войдите')}
        </AppContext.Consumer>
    )
}

export default CommentForm;