import React, { useEffect, useRef, useState } from 'react';
import styles from './Counter.module.scss';
import { AiOutlineLike, AiOutlineDislike } from 'react-icons/ai';
import { checkLikes } from '../../shared/utils';

type Props = {
    initialNum: number;
}

const Counter: React.FC<Props> = ({ initialNum }) => {

    const [count, setCount] = useState<number>(initialNum);
    const [message, setMessage] = useState<string>('');
    const [err, setError] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const initialization = useRef<boolean>(false);

    useEffect(() => {
        if (!initialization.current) {
            initialization.current = true;
            return;
        }
        /* Clear message before changing */
        clearMessage();
        setLoading(true);
        const checkAsync = async () => {
            try {
                const res = await checkLikes(count);
                setMessage(res);
            } catch (err) {
                setError(err);
            }
            setLoading(false);
        }

        checkAsync();
    }, [count]);

    const clearMessage = () => {
        setMessage('');
        setError('');
    }

    return (
        <div className={styles.Counter}>
            <p>Likes counter: </p>
            <p className={styles.counter__count}>{count}</p>
            <div className={styles.counter_buttons_container}>                <button
                disabled={loading}
                onClick={() => { setCount(count => count - 1) }}>
                <AiOutlineDislike size={20} />
            </button>
                <button
                    disabled={loading}
                    onClick={() => { setCount(count => count + 1) }}>
                    <AiOutlineLike size={20} />
                </button>
            </div>
            {message ? <div className={styles.counter__message}>{message}</div> : ''}
            {err && <div className={styles.counter__err}>{err}</div>}
            {loading && <div>Loading...</div>}
        </div>
    )
};

export default Counter;