import React from 'react';
import { Link } from 'react-router-dom';
import { Article } from '../../../shared/types';
import { getFormattedDayAndMonth } from '../../../shared/dateformat';
import styles from './ArticleElement.module.scss';

type Props = {
    article: Article;
}

const ArticleElement: React.FC<Props> = ({ article }) => {

    const formatted_creation_date = getFormattedDayAndMonth(article.created_at);

    return (
        <div className={styles.article__container}>
            <Link to={`/article/${article.id}`}>
                <article className={styles.ArticleElement}>
                    <div className={styles.article__content}>
                        <h3 className={styles.article__title}>{article.title}</h3>
                        <p className={styles.article__annotation}>{article.annotation}</p>
                        <div className={styles.article__footer}>
                            <span className={styles.article__date}>{formatted_creation_date}</span>
                        </div>
                    </div>
                    <div className={styles.article__image}>
                        <img src={`https://dev-darmedia-uploads.s3.eu-west-1.amazonaws.com/${article.image}`} 
                        alt={article.title}/>
                    </div>
                </article>
            </Link>
        </div>)
}

export default ArticleElement;