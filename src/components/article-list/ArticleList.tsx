import React from 'react';
import { Article } from '../../shared/types';
import ArticleElement from './article-element/ArticleElement';
import styles from './ArticleList.module.scss';

type Props = {
    articles: Article[];
}

const ArticleList: React.FC<Props> = ({ articles }) => {

    return (
        <div className={styles.articleList__container}>
            <h4 className={styles.articleList__header}>Актуальное</h4>
            <div className={styles.articleList}>
                {articles.map(article => (<ArticleElement key={article.id} article={article} />)
                )}
            </div>
        </div>
    )
};

export default ArticleList;