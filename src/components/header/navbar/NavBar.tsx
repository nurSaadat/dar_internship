import React from 'react';
import { Link } from 'react-router-dom';
import { Category } from '../../../shared/types';
import styles from './NavBar.module.scss';

type Props = {
    items: Category[];
}

const NavBar: React.FC<Props> = ({ items }) => {
    return (
        <div className={styles.NavBar}>
            <ul className={styles.NavBar__links}>
                <li><Link to='/articles'>Все статьи</Link></li>
                {items.map((item) =>
                (<li key={item.id}>
                    <Link to={`/articles/${item.id}`}>
                        {item.title}
                    </Link>
                </li>)
                )}

                <li><Link to='/counter'>Counter</Link></li>
            </ul>
        </div>
    );
};

export default NavBar;