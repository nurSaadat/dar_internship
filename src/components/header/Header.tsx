import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCategories } from '../../shared/redux/cathegories/categories.actions';
import { selectCategories } from '../../shared/redux/cathegories/categories.selectors';
import HeaderTop from './header-top/HeaderTop';
import NavBar from './navbar/NavBar';

const data = ['О нас', 'Обучение', 'Сообщество', 'Медиа'];

const Header: React.FC = () => {

    const categories = useSelector(selectCategories);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch])

    return (
        <div className='Header'>
            <HeaderTop items={data}/>
            <NavBar items={categories} />
        </div>
    );
};

export default Header;