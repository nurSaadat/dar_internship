import React from 'react';
import styled from 'styled-components';

type StyledButtonProps = {
    outline?: boolean;
}

const StyledButton = styled.button<StyledButtonProps>`
    color: ${props => props.outline ? '#7400b1' : 'white'};
    cursor: pointer;
    outline: none;
    border-radius: 5px;
    background: ${props => props.outline ? 'none' : '#7400b1'};
    border: ${props => props.outline ? '1px solid #7400b1' : 'none'};
    padding: 0.8rem 1.2rem;
    font-size: 16px;
    margin: 0.5rem;
`

const SecondaryStyledButton = styled(StyledButton)`
    color: ${props => props.outline ? 'pink' : 'white'};
    background: ${props => props.outline ? 'none' : 'pink'};
    border: ${props => props.outline ? '1px solid pink' : 'none'};
`

const GhostStyleButton = styled(StyledButton)`
    border: 1px solid #7400b1;
    color: #7400b1;
    background: none;

    &:hover {
        background: #7400b1;
        color: 'white';
    }
`

type Props = {
    title: string;
    outline?: boolean;
    variant?: 'primary' | 'secondary' | 'ghost';
    onClick?: () => void;
}

const getStyledButton = (variant: 'primary' | 'secondary' | 'ghost') => {
    switch (variant) {
        case 'primary':
            return StyledButton;
        case 'secondary':
            return SecondaryStyledButton;
        case 'ghost':
            return GhostStyleButton;
        default:
            return StyledButton;
    }
}

const Button: React.FC<Props> = ({ title, variant, outline, onClick }) => {

    const Styled = getStyledButton(variant || 'primary')

    return (
        // convert to boolean
        <Styled outline={!!outline} onClick={onClick}>
            {title}
        </Styled>
    )
}

export default Button;  