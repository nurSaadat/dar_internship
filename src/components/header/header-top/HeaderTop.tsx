import React, { useContext } from 'react';
import styles from './HeaderTop.module.scss';
import Button from '../button/Button';
import AppContext from '../../../shared/app.context';
import Profile from '../../profile/Profile';
import { useHistory } from 'react-router-dom';

type Props = {
    items: string[];
}

const HeaderTop: React.FC<Props> = ({ items }) => {

    const { state: { profile } } = useContext(AppContext);
    const history = useHistory();

    const goToLogin = () => {
        history.push('/login');
    }

    const goToRegister = () => {
        history.push('/register');
    }

    return (
        <div className={styles.headerTop}>
            <header>
                <div className={styles._logo} />
                <ul className={styles._links}>
                    {items.map((item, index) =>
                    (<li key={index}>
                        <a href='/'>{item}</a>
                    </li>)
                    )}
                </ul>
                {
                    (profile ?
                        <div className={styles._profile}>
                            <Profile username={profile.username} avatar={profile.avatar} />

                        </div >
                        :
                        <div className={styles._buttons_container}>
                            <Button title={'Log in'} onClick={goToLogin} />
                            <Button title={'Register'} onClick={goToRegister} variant={'secondary'} />
                        </div>)
                }
            </header>
        </div>
    );
};


export default HeaderTop;