import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import ArticleList from '../../components/article-list/ArticleList';
import { fetchArticles } from '../../shared/redux/articles/articles.actions';
import { selectArticles } from '../../shared/redux/articles/articles.selectors';

const ArticlesPage: React.FC = () => {

    const { categoryId } = useParams<{ categoryId: string }>();
    const articles = useSelector(selectArticles);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchArticles(categoryId));
    }, [categoryId, dispatch])

    return (
        <div className='ArticlesPage'>
            <ArticleList articles={articles} />
        </div>
    )
}

export default ArticlesPage;