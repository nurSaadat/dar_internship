import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Button from '../../components/header/button/Button';
import InputField from '../../components/input/InputField';
import { getProfile, login } from '../../shared/api';
import AppContext from '../../shared/app.context';
import { fetchProfileSuccess } from '../../shared/redux/profile/profile.actions';
import styles from './LoginPage.module.scss';

const LoginPage: React.FC = () => {
    const { dispatch } = useContext(AppContext);
    const history = useHistory();
    const [fields, setFields] = useState<{
        username: string;
        password: string;
    }>({username: '', password: ''});

    const fieldChange = (fieldName: string, value: any) => {
        setFields(oldState => ({
            ...oldState,
            [fieldName]: value
        }))
    };

    const handleLogin = async () => {
        if (fields.password && fields.username) {
            login(fields.username, fields.password)
                .then(res => {
                    if (res.data.token) {
                        localStorage.setItem('authToken', res.data.token)
                    }
                    return getProfile();
                })
                .then(res => {
                    dispatch(fetchProfileSuccess(res.data));
                    history.replace('/');
                })
        }
    }

    return (
        <div className={styles.wrapper}>
            <p>Log in</p>
            <InputField
                label={'Username'}
                name={'username'}
                value={fields.username}
                type={'text'}
                onChange={(innerValue) => fieldChange('username', innerValue)}
                isRequired={true} />
            <InputField
                label={'Password'}
                name={'password'}
                value={fields.password}
                type={'password'}
                onChange={(innerValue) => fieldChange('password', innerValue)}
                isRequired={true} />
            <Button title='sign in' variant='primary' onClick={handleLogin} />
        </div>

    )
}

export default LoginPage;