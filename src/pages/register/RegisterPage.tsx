import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Button from '../../components/header/button/Button';
import InputField from '../../components/input/InputField';
import { getProfile, register } from '../../shared/api';
import AppContext from '../../shared/app.context';
import { fetchProfileSuccess } from '../../shared/redux/profile/profile.actions';
import { Profile } from '../../shared/types';
import styles from '../login/LoginPage.module.scss';

const RegisterPage: React.FC = () => {
    const { dispatch } = useContext(AppContext);
    const history = useHistory();
    const [fields, setFields] = useState<Profile>({
        username: '',
        password: '',
        firstName: '',
        lastName: '',
        avatar: ''
    });

    const fieldChange = (fieldName: string, value: any) => {
        setFields(oldState => ({
            ...oldState,
            [fieldName]: value
        }))
    };

    const handleRegister = async () => {
        if (fields.username && fields.firstName && fields.lastName && fields.password) {
            register(fields.username, fields.password, fields.firstName, fields.lastName)
                .then(res => {
                    if (res.data.token) {
                        localStorage.setItem('authToken', res.data.token)
                    }
                    return getProfile();
                })
                .then(res => {
                    dispatch(fetchProfileSuccess(res.data));
                    history.replace('/');
                })
        }
    }


    return (
        <div className={styles.wrapper}>
            <p>Register</p>
            <InputField
                label={'Username'}
                name={'username'}
                value={fields.username}
                type={'text'}
                onChange={(innerValue) => fieldChange('username', innerValue)}
                isRequired={true} />
            <InputField
                label={'Password'}
                name={'password'}
                value={fields.password}
                type={'password'}
                onChange={(innerValue) => fieldChange('password', innerValue)}
                isRequired={true} />
            <InputField
                label={'First name'}
                name={'firstName'}
                value={fields.firstName}
                type={'text'}
                onChange={(innerValue) => fieldChange('firstName', innerValue)}
                isRequired={true} />
            <InputField
                label={'Last name'}
                name={'lastName'}
                value={fields.lastName}
                type={'text'}
                onChange={(innerValue) => fieldChange('lastName', innerValue)}
                isRequired={true} />
            <Button title='Register' variant='primary' onClick={handleRegister} />

        </div>
    )
}

export default RegisterPage;
