import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import CommentForm from '../../components/comment-form/CommentForm';
import { fetchArticle } from '../../shared/redux/articles/articles.actions';
import { selectArticle } from '../../shared/redux/articles/articles.selectors';
import styles from './ArticlePage.module.scss';

const ArticlePage: React.FC = () => {

    const { articleId } = useParams<{ articleId: string }>();
    const dispatch = useDispatch();
    const article = useSelector(selectArticle);

    useEffect(() => {
        if (articleId) {
            dispatch(fetchArticle(articleId));
        }
    }, [articleId, dispatch])

    return (
        <div className='ArticlePage'>
            {
                article && (
                    <div className={styles.article__content}>
                        <div className={styles.article__title}>
                            {article.title}
                        </div>
                        <div
                            className={styles.article__annotation}
                            dangerouslySetInnerHTML={{ __html: article.description }}>
                        </div>
                        <CommentForm />
                    </div>
                )
            }
        </div>
    )
}

export default ArticlePage;