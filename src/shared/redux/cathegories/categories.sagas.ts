import { CategoriesActionTypes, fetchCategoriesError, fetchCategoriesSuccess } from './categories.actions';
import { all, call, put, takeLatest } from 'redux-saga/effects'
import { getCategories } from '../../api';

export function* fetchCategoriesAsync() {
    try {
        const res = yield getCategories();
        yield put(fetchCategoriesSuccess(res.data));
    } catch (e) {
        yield put(fetchCategoriesError(e));
    }
}

export function* fetchCategoriesSaga() {
    yield takeLatest(CategoriesActionTypes.FETCH_CATEGORIES, fetchCategoriesAsync)
}

export function* categoriesSagas() {
    yield all([
        call(fetchCategoriesSaga)
    ]);
}