import { combineReducers } from 'redux';
import { articlesReduser } from './articles/articles.reducer';
import { categoriesReducer } from './cathegories/categories.reducer';
import { profileReducer } from './profile/profile.reducer';

const rootReducers = combineReducers({
    category: categoriesReducer,
    articles: articlesReduser,
    profile: profileReducer,
});

export type RootState = ReturnType<typeof rootReducers>;

export default rootReducers;
