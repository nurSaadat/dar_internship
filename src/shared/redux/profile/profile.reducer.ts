import { Profile } from "../../types";
import { ProfileAction, ProfileActionTypes } from "./profile.types";

export interface ProfileState {
    profile: Profile | null;
    error: string | null;
}

const defaultState = {
    profile: null,
    error: null
}

export const profileReducer = (state: ProfileState = defaultState, action: ProfileAction): ProfileState => {
    switch (action.type) {
        case ProfileActionTypes.FETCH_PROFILE_SUCCESS:
            return {
                ...state,
                profile: action.payload
            };
        case ProfileActionTypes.FETCH_PROFILE_ERROR:
            return {
                ...state,
                error: action.payload
            };
        case ProfileActionTypes.RESET_PROFILE:
            return {
                ...state,
                profile: null,
            };
        default:
            return state;

    }
}