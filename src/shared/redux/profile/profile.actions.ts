import { Profile } from "../../types";
import { FetchProfileAction, FetchProfileErrorAction, FetchProfileSuccessAction, ProfileActionTypes, ResetProfileAction } from "./profile.types";

export const fetchProfile = (token: string): FetchProfileAction => {
    return {
        type: ProfileActionTypes.FETCH_PROFILE,
        payload: token
    }
}

export const fetchProfileSuccess = (profile: Profile): FetchProfileSuccessAction => {
    return {
        type: ProfileActionTypes.FETCH_PROFILE_SUCCESS,
        payload: profile
    }
}

export const fetchProfileError = (error: string): FetchProfileErrorAction => {
    return {
        type: ProfileActionTypes.FETCH_PROFILE_ERROR,
        payload: error
    }
}

export const resetProfile = (): ResetProfileAction => {
    return {
        type: ProfileActionTypes.RESET_PROFILE,
        payload: null
    }
}