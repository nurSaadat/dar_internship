import { call, put, all, takeLatest } from "redux-saga/effects";
import { getProfile } from "../../api";
import { fetchProfileError, fetchProfileSuccess } from "./profile.actions";
import { FetchProfileAction, ProfileActionTypes } from "./profile.types";

export function* fetchProfileAsync() {
    try {
        const res = yield getProfile();
        yield put(fetchProfileSuccess(res));
    }
    catch (e) {
        yield put(fetchProfileError(e));
    }
}

export function* fetchProfileSaga() {
    yield takeLatest(ProfileActionTypes.FETCH_PROFILE, fetchProfileAsync);
}

export function* profileSagas() {
    yield all([
        call(fetchProfileSaga)
    ])
}