import { createSelector } from "reselect";
import { RootState } from "../root-reducers";

const selectProfileState = (state: RootState) => state.profile;

export const selectProfile = createSelector(
    [selectProfileState],
    (state) => state.profile
);