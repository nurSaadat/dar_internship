import { Profile } from "../../types";

export enum ProfileActionTypes {
  FETCH_PROFILE = "FETCH_PROFILE",
  FETCH_PROFILE_SUCCESS = "FETCH_PROFILE_SUCCESS",
  FETCH_PROFILE_ERROR = "FETCH_PROFILE_ERROR",
  RESET_PROFILE = "RESET_PROFILE",
}

export interface FetchProfileAction {
  type: ProfileActionTypes.FETCH_PROFILE;
  payload: string;
}

export interface FetchProfileSuccessAction {
  type: ProfileActionTypes.FETCH_PROFILE_SUCCESS;
  payload: Profile;
}

export interface FetchProfileErrorAction {
  type: ProfileActionTypes.FETCH_PROFILE_ERROR;
  payload: string;
}

export interface ResetProfileAction {
  type: ProfileActionTypes.RESET_PROFILE;
  payload: null;
}

export type ProfileAction =
  | FetchProfileAction
  | FetchProfileSuccessAction
  | FetchProfileErrorAction
  | ResetProfileAction;
