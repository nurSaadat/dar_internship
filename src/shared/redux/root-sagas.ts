import { all, call } from "redux-saga/effects";
import { articlesSagas } from "./articles/articles.sagas";
import { categoriesSagas } from "./cathegories/categories.sagas";
import { profileSagas } from "./profile/profile.sagas";

export default function* rootSaga() {
    yield all([
        call(categoriesSagas),
        call(articlesSagas),
        call(profileSagas),
    ])
}