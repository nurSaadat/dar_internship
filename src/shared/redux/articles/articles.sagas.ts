import { all, call, put, takeLatest } from "redux-saga/effects";
import { getArticle, getArticles } from "../../api";
import { fetchArticleError, fetchArticlesError, fetchArticlesSuccess, fetchArticleSuccess } from "./articles.actions";
import { ArticlesActionTypes, FetchArticleAction, FetchArticlesAction } from "./articles.types";


export function* fetchArticlesAsync(action: FetchArticlesAction) {
    try {
        const res = yield getArticles(action.payload);
        yield put(fetchArticlesSuccess(res.data));
    } catch (e) {
        yield put(fetchArticlesError(e));
    }
}

export function* fetchArticlesSaga() {
    yield takeLatest(ArticlesActionTypes.FETCH_ARTICLES, fetchArticlesAsync)
}

export function* articlesSagas() {
    yield all([
        call(fetchArticlesSaga),
        call(fetchArticleSaga)
    ])
}

export function* fetchArticleAsync(action: FetchArticleAction) {
    try {
        const res = yield getArticle(action.payload)
        yield put(fetchArticleSuccess(res.data));
    } catch (e) {
        yield put(fetchArticleError(e));
    }
}

export function* fetchArticleSaga() {
    yield takeLatest(ArticlesActionTypes.FETCH_ARTICLE, fetchArticleAsync)
}