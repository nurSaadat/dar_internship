import { Article } from "../../types";
import { ArticlesActionTypes, FetchArticleAction, FetchArticleErrorAction, FetchArticlesAction, FetchArticlesErrorAction, FetchArticlesSuccessAction, FetchArticleSuccessAction } from "./articles.types";

export const fetchArticles = (categoryId: string): FetchArticlesAction => {
    return {
        type: ArticlesActionTypes.FETCH_ARTICLES,
        payload: categoryId
    }
}

export const fetchArticlesSuccess = (articles: Article[]): FetchArticlesSuccessAction => {
    return {
        type: ArticlesActionTypes.FETCH_ARTICLES_SUCCESS,
        payload: articles
    }
}

export const fetchArticlesError = (error: string): FetchArticlesErrorAction => {
    return {
        type: ArticlesActionTypes.FETCH_ARTICLES_ERROR,
        payload: error
    }
}

export const fetchArticle = (categoryId: string): FetchArticleAction => {
    return {
        type: ArticlesActionTypes.FETCH_ARTICLE,
        payload: categoryId
    }
}

export const fetchArticleSuccess = (articles: Article): FetchArticleSuccessAction => {
    return {
        type: ArticlesActionTypes.FETCH_ARTICLE_SUCCESS,
        payload: articles
    }
}

export const fetchArticleError = (error: string): FetchArticleErrorAction => {
    return {
        type: ArticlesActionTypes.FETCH_ARTICLE_ERROR,
        payload: error
    }
}
