import { Article } from "../../types";
import { ArticlesAction, ArticlesActionTypes } from "./articles.types";

export interface ArticlesState {
    articles: Article[];
    article: Article | null;
    error: any;
}

const defaultState = {
    articles: [],
    article: null,
    error: null
}

export const articlesReduser = (state: ArticlesState = defaultState, action: ArticlesAction): ArticlesState => {
    switch (action.type) {
        case ArticlesActionTypes.FETCH_ARTICLES_SUCCESS:
            return {
                ...state,
                articles: action.payload
            }
        case ArticlesActionTypes.FETCH_ARTICLES_ERROR:
            return {
                ...state,
                error: action.payload
            }
        case ArticlesActionTypes.FETCH_ARTICLE_SUCCESS:
            return {
                ...state,
                article: action.payload
            }
        case ArticlesActionTypes.FETCH_ARTICLE_ERROR:
            return {
                ...state,
                error: action.payload
            }
        default:
            return state;
    }
}