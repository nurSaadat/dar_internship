import { createContext } from "react";
import { ProfileState } from "./redux/profile/profile.reducer";
import { ProfileAction } from "./redux/profile/profile.types";

export interface AppContext {
    state: ProfileState,
    dispatch: (action: ProfileAction) => void;
}

const AppContext = createContext<AppContext>({
    // Наверное есть другой способ задать дефолтный стейт
    state: {
        profile: null,
        error: null
    },
    dispatch: () => {},
})

export default AppContext;