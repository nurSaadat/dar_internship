export interface Category {
    id: number;
    title: string;
    sort: number;
}

export interface Article {
    id: number;
    title: string;
    annotation: string;
    description: string;
    slug: string;
    image: string;
    locale: string;
    visited: boolean;
    is_published: boolean;
    created_by: string;
    updated_by: string;
    created_at: string;
    updated_at: string;
    editors_choice: boolean;
    author_id: number;
    category_id: number;
    type_id: number;
    is_news: boolean;
    user_id: number;
    authors_id: number;
    is_leader: boolean;
    likesCount?: number;
    commentsCount?: number;
}

export interface Profile {
    username: string;
    firstName: string;
    lastName: string;
    avatar: string;
    password: string;
}