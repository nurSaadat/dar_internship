export const checkLikes = (count: number) => {
    return new Promise<string>((resolve, reject) => {
        setTimeout(() => {
            if (count > 5 || count < 0){
                reject('Like limit was reached!');
            }
            resolve('Like count changed');
        }, 3000);
    });
}

export const validateEmail = (email: string) => {
    return email.match('/.+@.+.+/');
}